const userCreateForm = document.getElementById("user-create-form")
const userCreateSubmitButton = userCreateForm.querySelector("button[type='submit']")

userCreateForm.addEventListener('submit', createUser)

function createUser(event){
    event.preventDefault();
    let username = document.getElementById('uname').value;
    let email = document.getElementById('email').value;
    let favoriteColor = document.getElementById('favoriteColor').value;
    let ssn = document.getElementById('ssn').value;
    let user = new User({
        username: username,
        email: email,
        favoriteColor: favoriteColor,
        ssn: ssn
    })
    
    user = JSON.stringify(user)
    
    sendToServer(user,'/api/user')
    
}

class User {
    constructor(options){
        this.username = options.username;
        this.email = options.email;
        this.favoriteColor = options.favoriteColor;
        this.ssn = options.ssn;
    }
}

function sendToServer(user,url){
    fetch(url,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: user
    }).then(res => {
        if (res.status === 201){
            alert (`Success`)
        } else if (res.status === 409){
            alert ('Username already taken')
        }
    });
}